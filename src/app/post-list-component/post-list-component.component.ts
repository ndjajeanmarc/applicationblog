import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.css']
})
export class PostListComponentComponent implements OnInit {

  @Input() postTitle: string;
  @Input() postContent: string;
  @Input() postDate: string;
  @Input() postLoveIts: number;

  constructor() { }

  OnLoveItPlus() {
    if (this.postLoveIts <= 0) { console.log(this.postLoveIts = 1); }

  }

  OnLoveItMinus() {
    if (this.postLoveIts >= 0) { console.log(this.postLoveIts = -1); }
  }

  ngOnInit() {
  }

}
