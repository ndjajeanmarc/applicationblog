import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  listPost = [
    {
      title: "Mon premier post",
      content: "are executed in Visual Studio to define part of the source code and other resources of your application.\n" +
      "Typically you would use several templates that read the data in a single input file or database",
      loveIts: 0,
      created_at: new Date()
    },
    {
      title: "Mon deuxieme post",
      content: "are executed in Visual Studio to define part of the source code and other resources of your application.\n" +
      "Typically you would use several templates that read the data in a single input file or database",
      loveIts: 0,
      created_at: new Date()
    },
    {
      title: "Encore un  post",
      content: "are executed in Visual Studio to define part of the source code and other resources of your application.\n" +
      "Typically you would use several templates that read the data in a single input file or database",
      loveIts: 0,
      created_at: new Date()
    },

  ];
}
